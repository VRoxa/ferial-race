FERIAL RACE

Integrants: Yeray Pérez García, Víctor De la Rocha.

Ferial Race es un juego de habilidad y concentración, donde el jugador deberá conseguir que su caballo de carreras llegue a la meta antes que sus rivales.

El caballo reacciona directamente a la performance del jugador, que deberá acertar las piezas que van apareciendo de forma aleatoria en la parte inferior de la pantalla.
La dificultad del juego aumenta gradualmente a medida que la carrera avanza.

En la mitad superior, se muestran las animaciones de los caballos, el background.

El jugador puede acumular bonus si lleva una racha de piezas acertadas.

Las piezas se aciertan con las teclas, asignando una tecla a una posición concreta por donde se deslizará la pieza.

Si la pieza pasa de largo antes que el jugador la acierte, se penalizará la velocidad del caballo.

EN LA CARPETA DOC SE PUEDE ENCONTRAR LA DOCUMENTACIÓN ASÍ COMO UN PEQUEÑO VÍDEO PARA ENSEÑAR COMO ES EL JUEGO.
